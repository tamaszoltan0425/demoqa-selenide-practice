package org.fasttrackit;

import com.codeborne.selenide.WebDriverRunner;

import static com.codeborne.selenide.Selenide.open;

public class Homepage {

    public static final String URL = "https://demoqa.com/";

    public void openHomepage() {
        System.out.println("Opening: " + URL);
        open(URL);
        WebDriverRunner.getWebDriver().manage().window().maximize();
    }

}
