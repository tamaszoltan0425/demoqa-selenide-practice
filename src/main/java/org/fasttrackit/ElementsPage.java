package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class ElementsPage {

    public static final String ELEMENTSURL = "https://demoqa.com/elements";

    public static final SelenideElement textBoxButton = $(".element-group:first-of-type #item-0");
    public static final SelenideElement checkBoxButton = $(".element-group:nth-of-type(1) #item-1");
    public static final SelenideElement radioButtonButton = $(".element-group:nth-of-type(1) #item-2");
    public static final SelenideElement buttonsButton = $(".element-group:nth-of-type(1) #item-4");


    public void openElements() {
        System.out.println("Opening: " + ELEMENTSURL);
        open(ELEMENTSURL);
    }

    public void clickTextBox() {
        textBoxButton.click();
    }

    public void clickCheckBox() {
        checkBoxButton.click();
    }

    public void clickRadioButton() {
        radioButtonButton.click();
    }

    public void clickButton() {
        buttonsButton.click();
    }

}
