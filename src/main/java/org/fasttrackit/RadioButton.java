package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class RadioButton {

    public static final SelenideElement yesRadio = $("input[id='yesRadio']")
            ;
    public static final SelenideElement impressiveRadio = $(".custom-radio #impressiveRadio");
    public static final SelenideElement noRadio = $(".custom-radio #noRadio");



    public void selectYes() {
        yesRadio.pressTab();
        yesRadio.sendKeys(" ");
        yesRadio.screenshot();
    }


//    public void clickImpressive() {
//        impressiveRadio.click();
//    }


}
