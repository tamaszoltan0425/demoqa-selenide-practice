package org.fasttrackit;


import static org.fasttrackit.Homepage.URL;

public class Main {
    public static void main(String[] args) {

        //opening DemoQA website
        System.out.println("Opening homepage: " + URL);
        Homepage homepage = new Homepage();
        homepage.openHomepage();

        //click on Elements card
        System.out.println("Performing click on 'Elements' link");
        ElementsPage elements = new ElementsPage();
        elements.openElements();

        //click Text Box button
        System.out.println("Performing click on 'Text Box' link");
        elements.clickTextBox();

        //fill in the form
        TextBoxUserForm userForm = new TextBoxUserForm();
        userForm.typeInName("Testdata Name");
        userForm.typeInEmail("test@data.test");
        userForm.setCurrentAddress("1 Test 2 Data");
        userForm.setPermanentAddress("Test 2 Data 3");
        userForm.clickSubmitButton();


        //check box
        elements.clickCheckBox();
        CheckBoxPage checkBoxPage = new CheckBoxPage();
        checkBoxPage.clickPlus();
        checkBoxPage.clickMinus();
        checkBoxPage.collapseExpand();
        System.out.println("-------- Testing if clicking the 'Home' checkbox checks the boxes for 'Desktop', 'Documents' and 'Downloads' ");
        checkBoxPage.clickHomeCheckBox();
        System.out.println("All boxes are checked!" + checkBoxPage.allChecked());


        //radio button
        elements.clickRadioButton();
        RadioButton radioButton = new RadioButton();
        radioButton.selectYes();




        //buttons
        //double click
        Buttons buttons = new Buttons();
        elements.clickButton();
        System.out.println("Performing double-click. ");
        buttons.setDoubleClick();


    }
}