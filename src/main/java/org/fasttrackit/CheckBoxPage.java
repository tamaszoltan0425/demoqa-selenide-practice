package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CheckBoxPage {

    public static final SelenideElement plusButton = $(".rct-icon-expand-all");
    public static final SelenideElement minusButton = $(".rct-icon-collapse-all");
    public static final SelenideElement collapseExpandButton = $(".rct-icon-expand-close");
    public static final SelenideElement homeCheckBox = $("#tree-node-home ~ .rct-checkbox");
    public static final SelenideElement desktopCheckBox = $("#tree-node-desktop ~ .rct-checkbox .rct-icon-uncheck");
    public static final SelenideElement documentCheckBox = $("#tree-node-documents ~ .rct-checkbox .rct-icon-uncheck");
    public static final SelenideElement downloadsCheckBox = $("#tree-node-downloads ~ .rct-checkbox .rct-icon-uncheck");

    public void clickPlus() {
        plusButton.click();
    }

    public void clickMinus() {
        minusButton.click();
    }

    public void collapseExpand(){
        collapseExpandButton.click();
    }

    public void clickHomeCheckBox() {
        homeCheckBox.click();
    }
    public void setDesktopCheckButtonChild() {
        desktopCheckBox.click();
    }

    public void setDocumentsCheckButtonChild() {
        documentCheckBox.click();
    }

    public void setDownloadsCheckButtonChild() {
        downloadsCheckBox.click();
    }

    public boolean allChecked() {
        return (!desktopCheckBox.exists() && !documentCheckBox.exists() && !downloadsCheckBox.exists());
    }


}
