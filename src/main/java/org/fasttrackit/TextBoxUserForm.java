package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class TextBoxUserForm {

    public static final SelenideElement fullName = $("#userName");
    public static final SelenideElement email = $("#userEmail");
    public static final SelenideElement currentAddress = $("#currentAddress");
    public static final SelenideElement permanentAddress = $("#permanentAddress");
    public static final SelenideElement submitButton = $("#submit");


    public void typeInName(String name) {
        fullName.click();
        fullName.type(name);
    }

    public void typeInEmail(String mail) {
        email.click();
        email.type(mail);
    }

    public void setCurrentAddress(String current) {
        currentAddress.click();
        currentAddress.type(current);
    }

    public void setPermanentAddress(String permaddress) {
        permanentAddress.click();
        permanentAddress.type(permaddress);
    }

    public void clickSubmitButton() {
        submitButton.click();
    }


}
