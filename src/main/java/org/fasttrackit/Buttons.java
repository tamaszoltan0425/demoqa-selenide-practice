package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Buttons {

    public static final SelenideElement doubleClick = $("#doubleClickBtn");

    public void setDoubleClick() {
        doubleClick.doubleClick();
    }

}
